----------------------------------------
--
-- Brainfuck Interpreter
--
-- author : Leonardone @ NEETSDKASU
-- license: MIT License 
--
----------------------------------------
module Main where


import Brainfuck
import System.Environment (getArgs, getProgName)
import System.IO          (readFile, stdin, stdout)


usage :: Compiler -> IO ()

usage compiler =
    do let get = unwords . getTokenList compiler
       prog <- getProgName
       putStrLn $ "Brainfuck Interpreter"
       putStrLn $ "  usage: " ++ prog ++ " <source file>"
       putStrLn $ ""
       putStrLn $ "  commands:"
       putStrLn $ "    increment pointer: " ++ get AddIndex
       putStrLn $ "    decrement pointer: " ++ get SubIndex
       putStrLn $ "    increment value  : " ++ get AddValue
       putStrLn $ "    decrement value  : " ++ get SubValue
       putStrLn $ "    begin of loop    : " ++ get While
       putStrLn $ "    end of loop      : " ++ get Wend
       putStrLn $ "    input from stdin : " ++ get Input
       putStrLn $ "    output to stdout : " ++ get Output
       putStrLn $ "    line comment     : " ++ get Comment


defaultCompiler :: Compiler

defaultCompiler = withComment  ["#", "//"] -- line comment
                $ buildCompiler
                $ set AddIndex [">", "Ri"] -- increment pointer
                $ set SubIndex ["<", "Le"] -- decrement pointer
                $ set AddValue ["+", "In"] -- increment value
                $ set SubValue ["-", "De"] -- decrement value
                $ set While    ["[", "Be"] -- begin of loop
                $ set Wend     ["]", "En"] -- end of loop
                $ set Input    [",", "Ch"] -- input from stdin
                $ set Output   [".", "Sh"] -- output to stdout
                $ newBuilder


main :: IO ()

main =
    do args <- getArgs
       let (u, t, f, c) = parseArgs args
       compiler <-
            case t of
            0 -> do return $ defaultCompiler
            1 -> do return $ standardCompiler
            2 -> do return $ standardCompilerWithComment
            3 -> do text  <- readFile c
                    return $ customCompiler text
       if u then
           do usage compiler
       else
           do source <- readFile f
              once stdin stdout $ compile compiler source


parseArgs :: [String] -> (Bool, Int, String, String)

parseArgs args = parse args (True, 0, "", "") where
    
    parse :: [String] -> (Bool, Int, String, String) -> (Bool, Int, String, String)
    
    parse a res@(u, t, f, c) =
        case a of
        []             -> res
        (""       :xs) -> parse xs res
        ("-d"     :xs) -> parse xs (u, 0, f, c)         -- use defaultCompiler
        ("-s"     :xs) -> parse xs (u, 1, f, c)         -- use standardCompiler
        ("-sw"    :xs) -> parse xs (u, 2, f, c)         -- use standardCompilerWithComment
        ("-c":cmds:xs) -> parse xs (u, 3, f, cmds)      -- use customCompiler
        ("-u"     :xs) -> parse xs (True, t, f, c)      -- show usage
        ("-f":file:xs) -> parse xs (False, t, file, c)  -- source file
        (file     :xs) -> parse xs (False, t, file, c)  -- source file


customCompiler :: String -> Compiler

customCompiler text =
    let cmds = map words $ lines text ++ repeat ""
        cmpl = withComment  (cmds !! 8)  -- line comment
             $ buildCompiler
             $ set AddIndex (cmds !! 0)  -- increment pointer
             $ set SubIndex (cmds !! 1)  -- decrement pointer
             $ set AddValue (cmds !! 2)  -- increment value
             $ set SubValue (cmds !! 3)  -- decrement value
             $ set While    (cmds !! 4)  -- begin of loop
             $ set Wend     (cmds !! 5)  -- end of loop
             $ set Input    (cmds !! 6)  -- input from stdin
             $ set Output   (cmds !! 7)  -- output to stdout
             $ newBuilder
    in cmpl
