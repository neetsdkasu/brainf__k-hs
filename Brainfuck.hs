----------------------------------------
--
-- Brainfuck Interpreter Core
--
-- author : Leonardone @ NEETSDKASU
-- license: MIT License 
--
----------------------------------------
module Brainfuck (
    
    TokenType(AddIndex, SubIndex, AddValue, SubValue, While, Wend, Input, Output, Comment),
    Machine, Builder, Compiler, Code,
    
    Command(AddIndexCommand, AddValueCommand, WhileCommand, InputCommand, OutputCommand),
    
    newMachine,
    once,
    run,
    newBuilder,
    set,
    buildCompiler,
    standardCompiler,
    standardCompilerWithComment,
    withComment,
    getTokenList,
    compile,
    decompile
    
    ) where


import Data.Char  (chr, ord)
import Data.List  (find, isPrefixOf, sortBy)
import Data.Map   ((!))
import Data.Ord   (comparing)
import Data.Tuple (swap)
import System.IO  (Handle, hGetChar, hPutChar, hFlush)
import qualified Data.IntMap as Memory
import qualified Data.Map as Map

type InputHandle  = Handle
type OutputHandle = Handle
           
type Code     = [Command]
type Index    = Int
type Memory   = Memory.IntMap Int
type JumpList = [Code]

type TokenRef = Map.Map TokenType [String]

data TokenType = AddIndex
               | SubIndex
               | AddValue
               | SubValue
               | While
               | Wend
               | Input
               | Output
               | Comment
               deriving (Show, Eq, Ord)

data Builder  = Builder (Map.Map String TokenType) deriving Show

data Compiler = Compiler [(String, TokenType)] deriving Show

data Machine = Machine Index Memory deriving Show

data Command = AddIndexCommand Int
             | AddValueCommand Int
             | WhileCommand    Code
             | InputCommand
             | OutputCommand
             deriving Show


newMachine :: Machine

newMachine = Machine 0 Memory.empty


once :: InputHandle -> OutputHandle -> Code -> IO ()

once inputH outputH code = 
    do run inputH outputH newMachine code
       return ()


run :: InputHandle -> OutputHandle -> Machine -> Code -> IO Machine

run inputH outputH (Machine index mem) = execute index mem [] where
    
    execute :: Index -> Memory -> JumpList -> Code -> IO Machine
    
    execute index mem jump code =
        case (jump, code) of
        (        [], []) -> do return (Machine index mem)
        ((ret:jump), []) -> do execute index mem jump ret
        (_, (AddIndexCommand p:cs)) -> do execute (index + p) mem jump cs
        (_, (AddValueCommand p:cs)) -> do let mem2 = Memory.insertWith (+) index p mem
                                          execute index mem2 jump cs
        (_, (WhileCommand loop:cs)) -> do if Memory.findWithDefault 0 index mem == 0
                                          then execute index mem jump cs
                                          else execute index mem (code:jump) loop
        (_, (InputCommand :cs)) -> do ch <- hGetChar inputH
                                      let mem2 = Memory.insert index (ord ch) mem
                                      execute index mem2 jump cs
        (_, (OutputCommand:cs)) -> do hPutChar outputH $ chr $ Memory.findWithDefault 0 index mem
                                      hFlush   outputH
                                      execute index mem jump cs


newBuilder :: Builder

newBuilder = Builder Map.empty           


set :: TokenType -> [String] -> Builder -> Builder

set tokentype tokens (Builder alltokens) =
    let insert' m token = Map.insert token tokentype m
        alltokens2 = foldl insert' alltokens $ filter (/= "") tokens
    in  Builder alltokens2


buildCompiler :: Builder -> Compiler

buildCompiler (Builder alltokens) =
    let tokenlist = sortBy (comparing (negate.length.fst)) $ Map.toList alltokens
    in  Compiler tokenlist


standardCompiler :: Compiler

standardCompiler = buildCompiler
                 $ set AddIndex [">"]
                 $ set SubIndex ["<"]
                 $ set AddValue ["+"]
                 $ set SubValue ["-"]
                 $ set While    ["["]
                 $ set Wend     ["]"]
                 $ set Input    [","]
                 $ set Output   ["."]
                 $ newBuilder


standardCompilerWithComment :: Compiler

standardCompilerWithComment = withComment ["#"] standardCompiler

                
withComment :: [String] -> Compiler -> Compiler

withComment toks (Compiler alltokens) =
    let newtokens = map (swap . curry id Comment) $ filter (/= "") toks
    in  Compiler (newtokens ++ alltokens)


getTokenList :: Compiler -> TokenType -> [String]

getTokenList (Compiler alltokens) typ = map fst $ filter ((== typ) . snd) alltokens

    
compile :: Compiler -> String -> Code

compile (Compiler alltokens) = parse [] [] . tokenize [] where
    
    tokenize :: [TokenType] -> String -> [TokenType]
    
    tokenize ts [] = ts
    
    tokenize ts xs =
        case find (flip isPrefixOf xs . fst) alltokens of
        Just (  _, Comment) -> tokenize ts       (dropWhile (\c -> c /= '\n' && c /= '\r') xs)
        Just (tok, typ)     -> tokenize (typ:ts) (drop (length tok) xs)
        Nothing             -> tokenize ts       (tail xs)
    
    
    parse :: Code -> [Code] -> [TokenType] -> Code
    
    parse code [] []= code
    
    parse _    _  [] = error "not found While-Wend pair"
    
    parse code stack (typ:ts) =
        case (typ, code, stack) of
        (Input,  _,  _) -> parse (InputCommand:code)  stack ts
        (Output, _,  _) -> parse (OutputCommand:code) stack ts
        (Wend,   _,  _) -> parse [] (code:stack) ts
        (While,  _, []) -> error "not found While-Wend pair"
        (While,  _, (rest:stack)) -> parse (WhileCommand code:rest) stack ts
        (AddIndex, (AddIndexCommand (-1):cs), _) -> parse cs stack ts
        (AddIndex, (AddIndexCommand    p:cs), _) -> parse (AddIndexCommand (p + 1):cs) stack ts
        (AddIndex,                        cs, _) -> parse (AddIndexCommand       1:cs) stack ts
        (SubIndex, (AddIndexCommand    1:cs), _) -> parse cs stack ts 
        (SubIndex, (AddIndexCommand    p:cs), _) -> parse (AddIndexCommand (p - 1):cs) stack ts
        (SubIndex,                        cs, _) -> parse (AddIndexCommand    (-1):cs) stack ts
        (AddValue, (AddValueCommand (-1):cs), _) -> parse cs stack ts
        (AddValue, (AddValueCommand    p:cs), _) -> parse (AddValueCommand (p + 1):cs) stack ts
        (AddValue,                        cs, _) -> parse (AddValueCommand       1:cs) stack ts
        (SubValue, (AddValueCommand    1:cs), _) -> parse cs stack ts 
        (SubValue, (AddValueCommand    p:cs), _) -> parse (AddValueCommand (p - 1):cs) stack ts
        (SubValue,                        cs, _) -> parse (AddValueCommand    (-1):cs) stack ts
        
        
decompile :: Compiler -> Code -> String

decompile compiler = convert tokens where
    
    tokens :: TokenRef
    
    tokens = Map.fromList
           $ map (\typ -> (typ, cycle $ case getTokenList compiler typ of
                                        []   -> [""]
                                        toks -> toks ))
           $ [AddIndex, SubIndex, AddValue, SubValue, While, Wend, Input, Output]
    
    
    getToken :: TokenRef -> TokenType -> String
    
    getToken tokens typ = head $ tokens ! typ
    
    
    rotTokens :: TokenRef -> [TokenType] -> TokenRef
    
    rotTokens tokens typs = foldl (flip (Map.adjust tail)) tokens typs
    
    
    convert :: TokenRef -> Code -> String
    
    convert _ [] = ""
    
    convert tokens (command:cs) = fragment ++ convert tokens2 cs where
        
        get = getToken tokens
        rot = rotTokens tokens
        
        (tokens2, fragment) =
            case command of
            AddIndexCommand p -> if p < 0
                                 then (rot [SubIndex], concat $ replicate (abs p) $ get SubIndex)
                                 else (rot [AddIndex], concat $ replicate p       $ get AddIndex)
            AddValueCommand p -> if p < 0
                                 then (rot [SubValue], concat $ replicate (abs p) $ get SubValue)
                                 else (rot [AddValue], concat $ replicate p       $ get AddValue)
            WhileCommand loop -> (rot [While, Wend], get While ++ convert tokens loop ++ get Wend)
            InputCommand      -> (rot [Input], get Input)
            OutputCommand     -> (rot [Output], get Output)
